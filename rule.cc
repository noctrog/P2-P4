#include "rule.h"
#include <stdexcept>
#include <cctype>

namespace P4
{
  BadRuleException::BadRuleException (const string& r) : runtime_error(r)
  {

  }

  Rule::Rule(string r)
  {
    try
    {
      if (r.length() != 4)
        throw BadRuleException("Rule: size mismatch.");
      for (auto it : r){
        if (TODIGIT(it) < 0 || TODIGIT(it) > 8)
          throw BadRuleException("Rule: no digit in rule.");
      }

      // Si todo esta bien
      minSurvive = TODIGIT(r.at(0));
      maxSurvive = TODIGIT(r.at(1));
      minBorn = TODIGIT(r.at(2));
      maxBorn = TODIGIT(r.at(3));
      theRule = r;
    }catch (BadRuleException& e){
      throw;
    }catch(...){
      throw;
    }
  }

  Rule::~Rule()
  {

  }

  int Rule::get_min_survive()
  {
    return minSurvive;
  }

  int Rule::get_max_survive()
  {
    return maxSurvive;
  }

  int Rule::get_min_born()
  {
    return minBorn;
  }

  int Rule::get_max_born()
  {
    return maxBorn;
  }

  string Rule::get_rule ()
  {
    return theRule;
  }

  bool Rule::dies (int n)
  {
    return (n < minSurvive || n > maxSurvive) ? true : false;
  }

  bool Rule::survives (int n)
  {
    return !dies(n);
  }

  bool Rule::is_born (int n)
  {
    return (n >= minBorn && n <= maxBorn) ? true : false;
  }

  bool Rule::is_digit(char c)
  {
    return isdigit(c);
  }
}
