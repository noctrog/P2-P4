#include "world.h"

namespace P4
{
  World::World() : nbeings(0)
  {

  }
  World::~World ()
  {

  }

  size_t World::num_beings ()
  {
    return nbeings;
  }
}
