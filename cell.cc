#include "cell.h"

namespace P4
{
  Cell::Cell() : theWorld(nullptr)
  {

  }

  Cell::~Cell()
  {

  }

  World2dPtr Cell::getWorld2d()
  {
    return theWorld;
  }

  void Cell::setWorld2d(World2dPtr w)
  {
    theWorld = w;
  }
}
