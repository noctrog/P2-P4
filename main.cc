#include "cell.h"
#include "world2d.h"
#include <iostream>

void onCellDiesCallback(P4::CellPtr which, size_t x, size_t y)
{
	std::cout << "Ha muerto la celula en: (" << x << "," << y << "), con "
	<< which->get_epochs() << " anios." << std::endl;
}

void onCellIsBornCallback(P4::CellPtr which, size_t x, size_t y)
{
	std::cout << which->is_alive() << "Ha nacido la celula en: (" << x << "," << y << ") " << std::endl;
}

void onCellSurvivesCallback(P4::CellPtr which, size_t x, size_t y)
{
	std::cout << which->is_alive() << "Ha sobrevivido la celula en: (" << x << "," << y << ") " << std::endl;
}

void onEvolveIterationCallback(size_t generation, P4::World2dPtr w)
{
	std::cout << "--------------------------------------" << std::endl;
	std::cout << *w;
	std::cout << "Numero de celulas: " << w->num_beings() << std::endl << std::endl;
}

void onEpochCallback(P4::LifeBeingPtr p)
{
	std::cout << "La celula ha sobrevivido y tiene " << p->get_epochs() << " anios" << std::endl;
}

int main(int argc, char **argv)
{
	P4::World2d w ("2333", 10, 10);
	w.onCellDies.connect(onCellDiesCallback);
	w.onEvolveIteration.connect(onEvolveIterationCallback);
	w.onCellIsBorn.connect(onCellIsBornCallback);
	w.onCellSurvives.connect(onCellSurvivesCallback);

	// Primer caminante
	P4::CellPtr d = new P4::Cell();
	d->onEpoch.connect(onEpochCallback);
	w.insert_cell_at(new P4::Cell, 5, 5);
	w.insert_cell_at(new P4::Cell, 5, 6);
	w.insert_cell_at(new P4::Cell, 5, 7);
	w.insert_cell_at(d, 6, 7);
	w.insert_cell_at(new P4::Cell, 7, 6);
	w.insert_cell_at(new P4::Cell, -3, 0);
	w.insert_cell_at(new P4::Cell, 10, 10000);
	w.evolve(2);
	d->set_alive(false);
	std::cout << "Se ha eliminado la celula D. El mapa queda asi: \n" << w.to_string();
	w.evolve(8);

	P4::World2d::CellRow c = w[-100000];
	c = w[100000];
	c = w[w.cols()];

	std::cout << "Se lee el mapa y se elimina el anterior\n";
	w.read_from_file("../mapa.txt");
	std::cout << "Mapa cargado:\n" << w;
	w.evolve(50);

	return 0;
}
