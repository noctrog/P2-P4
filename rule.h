/*
 * Copyright (C) 2018
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RULE_H
#define RULE_H

#include <exception>
#include <string>
#include <iostream>
#include "utils.h"

using namespace std;

namespace P4 {

  DECPTR(Rule);

  class BadRuleException: public runtime_error
  {
  public:
    BadRuleException (const string& r);
  };

  class Rule
  {
  public:
    Rule(string r);
    virtual ~Rule();

    int get_min_survive();
    int get_max_survive();
    int get_min_born();
    int get_max_born();

    string get_rule ();

    bool dies (int n);
    bool survives (int n);
    bool is_born (int n);

  private:
    string theRule;
    int minSurvive;
    int maxSurvive;
    int minBorn;
    int maxBorn;

    bool is_digit (char c);

  };
} // P4

#endif /* RULE_H */
