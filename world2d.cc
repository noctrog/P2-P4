#include "cell.h"
#include "world2d.h"

namespace P4
{
  World2d::World2d (const string& r, size_t w, size_t h) : theRule(r)
  {
    for (size_t i = 0; i < h; ++i){
      CellRow aux;
      for (size_t j = 0; j < w; ++j){
        aux.push_back(nullptr);
      }
      theBoard.push_back(aux);
    }
  }

  World2d::~World2d ()
  {
    clear_board();
  }

  bool    World2d::insert_cell_at (CellPtr c, size_t x, size_t y)
  {
    bool success = true;
    if (c == nullptr){
      success = false;
    }
    else if (x < theBoard.at(0).size() && y < theBoard.size())
    {
      if (theBoard[y][x] != nullptr){
        success = false;
        delete c;
      }
      else
      {
        theBoard[y][x] = c;
        c->setWorld2d(this);
        nbeings++;
      }
    }
    else{
      delete c;
      success = false;
    }

    return success;
  }

  CellPtr World2d::get_cell_at (size_t x, size_t y)
  {
    if (y >= theBoard.size() || x >= theBoard.at(0).size())
      return nullptr;
    else
      return theBoard[y][x];
  }

  void    World2d::get_cell_coords (CellPtr c, size_t& x, size_t& y)
  {
    x = NOPOS; y = NOPOS;
    bool bFound = false;
    for (size_t row = 0; row < theBoard.size() && !bFound; ++row){
      for (size_t column = 0; column < theBoard.at(0).size(); ++column){
        if (c == theBoard[row][column]){
          bFound = true;
          x = column;
          y = row;
          break;
        }
      }
    }
  }

  size_t World2d::rows ()
  {
    return theBoard.size();
  }

  size_t World2d::cols ()
  {
    return theBoard.at(0).size();
  }

  string World2d::to_string ()
  {
    string str;

    for (auto row : theBoard){
      for (auto column : row){
        str += (column == nullptr || not column->is_alive()) ? NOCELL : CELL;
      }
      str += '\n';
    }
    str += '\n';

    return str;
  }

  void World2d::read_from_file (const string& fn)
  {
    ifstream ifs (fn);
    read_from(ifs);
  }

  void World2d::read_from (ifstream& ifs)
  {
    if (ifs.is_open() && ifs.good())
    {
      // eliminar el tablero anterior
      clear_board();
      // eliminar el tablero auxiliar
      for (auto row : auxBoard){
        row.clear();
      }
      auxBoard.clear();

      string row_s;
      while (getline(ifs, row_s) && row_s != "")
      {
        CellRow cellrow;
        auxBoard.push_back(cellrow);
        for (auto c : row_s){
          if (c == NOCELL){
            cellrow.push_back(nullptr);
          }
          else{
            cellrow.push_back(new Cell);
            cellrow.back()->setWorld2d(this);
            nbeings++;
          }
          auxBoard.back().push_back(nullptr);
        }

        theBoard.push_back(cellrow);
      }
    }
  }

  void World2d::evolve (size_t maxgen)
  {
    size_t gen;
    for (gen = 0; gen < maxgen && nbeings != 0; ++gen)
    {
      // Clear aux board
      for (auto row : auxBoard){
        row.clear();
      }
      auxBoard.clear();

      for (size_t y = 0; y < theBoard.size(); ++y)
      {
        auxBoard.push_back({});
        std::vector<CellPtr> aux;
        for (size_t x = 0; x < theBoard.at(0).size(); ++x)
        {
          int nb = num_neighbours(x, y);

          // Si la casilla esta vacia
          if (theBoard[y][x] == nullptr)
          {
            if (theRule.is_born(nb))
            {
              auxBoard.back().push_back(new Cell);
              auxBoard.back().back()->setWorld2d(this);
              nbeings++;
            }
            else
            {
              auxBoard.back().push_back(nullptr);
            }
          }
          else
          {
            if (theRule.survives(nb))
            {
              auxBoard.back().push_back(theBoard[y][x]);
              auxBoard.back()[x]->pass_epoch();
            }
            else
            {
              auxBoard.back().push_back(nullptr);
              theBoard[y][x]->pass_epoch();
              theBoard[y][x]->set_alive(false);
              nbeings--;
            }
          }
          evolve_cell_at(x, y);
        }
      }

      update_board();
      onEvolveIteration(gen, this);
    }

    update_board();
    onEvolveFinished(gen, this);
  }

  void World2d::clear_board ()
  {
    for (auto row : theBoard){
      for (auto cell : row){
        if (cell){
          delete cell;
          cell = nullptr;
        }
        row.clear();
      }
    }
    theBoard.clear();
    nbeings = 0;
  }

  size_t World2d::num_neighbours (size_t x , size_t y)
  {
    size_t nb = 0;

    if (x > theBoard.at(0).size() || y > theBoard.size()) return 0;

    for (int i = -1; i < 2; ++i){
      for (int j = -1; j < 2; ++j){
        if ((!(i == 0 && j == 0)) && static_cast<int>(y) + i >= 0 &&
            static_cast<int>(y) + i < static_cast<int>(theBoard.size()) &&
            static_cast<int>(x) + j < static_cast<int>(theBoard.at(0).size()) &&
            static_cast<int>(x) + j >= 0)
        {
          // Si la casilla no es la propia y esta dentro de los limites del mapa
          if (theBoard[y+i][x+j] != nullptr){
            nb++;
          }
        }
      }
    }

    return nb;
  }

  void World2d::evolve_cell_at (size_t x , size_t y)
  {
    int nb = num_neighbours(x, y);

    // Si la casilla esta vacia
    if (theBoard[y][x] == nullptr)
    {
      if (theRule.is_born(nb))
      {
        onCellIsBorn(auxBoard[y][x], x, y);
      }
    }
    else
    {
      if (theRule.survives(nb))
      {
        onCellSurvives(theBoard[y][x], x, y);
      }
      else
      {
        onCellDies(theBoard[y][x], x, y);
      }
    }
  }

  void World2d::update_board ()
  {
    for (auto row : theBoard){
      for (auto column : row)
        if (column)
          if (not column->is_alive())
            delete column;
    }
    theBoard.clear();

    theBoard = auxBoard;
  }

  World2d::CellRow& World2d::operator[] (size_t i)
  {
    if (i < 0)                      return theBoard.front();
    else if (i >= theBoard.size())  return theBoard.back();
    else                            return theBoard.at(i);
  }

  ostream& operator<< (ostream& os, const World2d& w)
  {
    for (auto row : w.theBoard){
      for (auto column : row){
        if (column && column->is_alive()) os << World2d::CELL;
        else        os << World2d::NOCELL;
      }
      os << '\n';
    }
    os << '\n';
    return os;
  }
}
