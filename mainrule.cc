#include <iostream>
#include "rule.h"
using namespace std;
using namespace P4;
int main(){
	Rule r("2132");

	cout << r.get_min_survive() << endl;
	cout << r.get_max_survive() << endl;
	cout << r.get_min_born() << endl;
	cout << r.get_max_born() << endl;
	
	// Rule r3("32423423");
	string cadena;
	cout << "Introduce la cadena: ";
	getline(cin, cadena);
	try{
		cout << "empezando try" << endl;
		Rule r2(cadena);
		cout << "esto no sale" << endl;
		cout << r2.get_min_survive() << endl;
	}
	catch(BadRuleException &e){
		cout << "capturando excepcion" << e.what() << endl;
	}

	cout << "esto si sale" << endl;
	
	try {
		Rule r2("199a");
		cout << "esto no sale" << endl;
	}
	catch(BadRuleException &e){
		cout << "capturando excepcion" << e.what() << endl;
	}
	cout << "adios" << endl;
	return 0;
}
