#include "world2d.h"
#include "cell.h"
#include <iostream>
using namespace std;

using namespace P4;

int main(){
	World2d wp("2333", 4, 4);
	

	wp.insert_cell_at(new Cell(),  0, 0);
	wp.insert_cell_at(new Cell(),  1, 0);
	wp.insert_cell_at(new Cell(),  2, 0);
	wp.insert_cell_at(new Cell(),  3, 0);
	
	cout << wp << endl;
	
	wp.evolve(1);

	cout << wp << endl;


	

	// "min-survive max-surive min-born max-born"
	World2d  w("2333", 5, 8);
	cout << w << endl;
	//cout << w.to_string() << endl;

	w.insert_cell_at(new Cell(), -1, -1);
	w.insert_cell_at(new Cell(),  3, 2);
	w.insert_cell_at(new Cell(),  0, 0);
	w.insert_cell_at(new Cell(),  1, 0);
	w.insert_cell_at(new Cell(),  2, 0);
	w.insert_cell_at(new Cell(),  0, 1);
	w.insert_cell_at(new Cell(),  0, 2);
	


	w.insert_cell_at(new Cell(),  0, 4);
	w.insert_cell_at(new Cell(),  1, 4);
	w.insert_cell_at(new Cell(),  0, 5);
	w.insert_cell_at(new Cell(),  1, 5);


	cout << w << endl;

	w.evolve(4);

	cout << w << endl;

	w.insert_cell_at(new Cell(),  0, 6);
	w.insert_cell_at(new Cell(),  1, 6);
	w.insert_cell_at(new Cell(),  2, 6);
	w.insert_cell_at(new Cell(),  3, 6);

	cout << w << endl;

	w.evolve(1);

	cout << w << endl;


	
	return 0;
}
