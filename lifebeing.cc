#include "lifebeing.h"

namespace P4
{
  LifeBeing::LifeBeing() : alive(true), epochs(0)
  {

  }

  LifeBeing::~LifeBeing()
  {

  }

  void LifeBeing::pass_epoch ()
  {
    if (alive){
      epochs++;
      onEpoch(this);
    }
  }

  void LifeBeing::set_alive (bool a)
  {
    alive = a;
  }

  bool LifeBeing::is_alive ()
  {
    return alive;
  }

  epoch_t LifeBeing::get_epochs ()
  {
    return epochs;
  }
}
