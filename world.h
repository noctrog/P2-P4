/* -*- mode: c++ -*-
 * Copyright (C) 2018
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WORLD_H
#define WORLD_H

using namespace std;

#include <iostream>
#include <fstream>
#include "utils.h"

namespace P4 {

  DECPTR(World);
  
  class World
  {
  public:
    World();
    virtual ~World ();

    size_t num_beings ();

    virtual void read_from (ifstream& ifs) = 0;
    virtual void evolve (size_t maxgen) = 0;

  protected:
    size_t nbeings;
  };


}  // P4

#endif /* WORLD_H */
