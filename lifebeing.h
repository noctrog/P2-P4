/* -*- mode: c++ -*-
 * Copyright (C) 2018
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIFEBEING_H
#define LIFEBEING_H

#include <boost/signals2.hpp>
#include "utils.h"

namespace P4 {
  typedef unsigned long epoch_t;

  DECPTR(LifeBeing);
  
  class LifeBeing
  {
  public:
    LifeBeing ();
    virtual ~LifeBeing ();

    void pass_epoch ();
    void set_alive (bool a);
    bool is_alive ();
    epoch_t get_epochs ();

    boost::signals2::signal<void (LifeBeingPtr)> onEpoch;
    
  private:
    bool alive;
    epoch_t epochs;
  };


}  // P4

#endif /* LIFEBEING_H */
