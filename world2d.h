/* -*- mode: c++ -*-
 * Copyright (C) 2018
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vector>
#include <string>
#include <boost/signals2.hpp>
#include <limits>

#include "world.h"
#include "utils.h"
#include "rule.h"

using namespace std;

namespace P4 {

  DECPTR(Cell);
  DECPTR(World2d);

  class World2d : public World
  {
  public:
    World2d (const string& r, size_t w, size_t h);
    virtual ~World2d ();

    bool    insert_cell_at (CellPtr c, size_t x, size_t y);
    CellPtr get_cell_at (size_t x, size_t y);
    void    get_cell_coords (CellPtr c, size_t& x, size_t& y);

    size_t rows ();
    size_t cols ();
    string to_string ();

    void read_from_file (const string& fn);
    void read_from (ifstream& ifs);

    void evolve (size_t maxgen = 100);

    ///////////
    // Types //
    ///////////------------------------------------------------------------
    using CellRow = vector<CellPtr>;
    // Si la linea anterior te da problemas de compilacion, comentala y
    // usa la siguiente.
    // typedef vector<CellPtr> CellRow;

    // Similar con esta otra.
    using Board = vector<CellRow>;
    // typedef vector<CellRow> Board;
    ///////////------------------------------------------------------------

    ///////////////
    // Operators //
    ///////////////--------------------------------------------------------
    CellRow& operator[] (size_t i);
    friend ostream& operator<< (ostream& os, const World2d& w);
    ///////////////--------------------------------------------------------

    ///////////////
    // Constants //
    ///////////////--------------------------------------------------------
    static const char CELL = '*';
    static const char NOCELL = '.';
    static const size_t NOPOS = std::numeric_limits<size_t>::max();
    ///////////////--------------------------------------------------------

    /////////////
    // Signals //
    /////////////----------------------------------------------------------
    boost::signals2::signal<void (size_t , World2dPtr)> onEvolveIteration;
    boost::signals2::signal<void (size_t , World2dPtr)> onEvolveFinished;
    boost::signals2::signal<void (CellPtr , size_t, size_t)> onCellSurvives; // Cell, x, y
    boost::signals2::signal<void (CellPtr , size_t, size_t)> onCellDies; // Cell, x, y
    boost::signals2::signal<void (CellPtr , size_t, size_t)> onCellIsBorn; // Cell, x, y
    /////////////----------------------------------------------------------

  protected:
    Rule theRule;
    Board theBoard;
    Board auxBoard;

    void clear_board ();
    size_t num_neighbours (size_t x , size_t y);
    void evolve_cell_at (size_t x , size_t y);
    void update_board ();
  };


}  // P4
