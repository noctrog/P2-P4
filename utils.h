// -*- mode: c++ -*-

// Compilar con --std=c++11 o superior

#ifndef UTILS
#define UTILS


//////////////////////////////////////////////////////////////////////
// Esta macro genera para un tipo dado 'C' su tipo 'puntero': CPtr. //
// Asumimos que los valores que toma 'C' solo seran clases.         //
//////////////////////////////////////////////////////////////////////

#define DECPTR(C) class C; typedef C* C##Ptr



///////////////////////////////////////////////////////////////////
// Convierte un caracter que debe ser un digito en el número que //
// representa el carácter.                                       //
///////////////////////////////////////////////////////////////////

#define TODIGIT(c) (c-'0')

#endif
